var clientesObtenidos;
function obtenerClientes()
{
//    var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers?$filter=Country eq 'Germany'";
    var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";

    var request = new XMLHttpRequest();

    request.onreadystatechange = function () {
      if (this.readyState==4 && this.status == 200 ) {
        console.log( request.responseText);
        clientesObtenidos=request.responseText;
        procesarClientes();
      }
    }

    request.open("GET", url, true);
    request.send();

}


function procesarClientes()
{
  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  var extension = ".png";

  var JSONClientes = JSON.parse (clientesObtenidos);
//  alert(JSONProductos.value[0].ProductName);

  var divClientes = document.getElementById("divClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");
  var thead = document.createElement("thead");
  var theadTR = document.createElement("tr");

  var nameContactName = document.createElement("th");
  nameContactName.innerText = "Contact Name";
  var nameCity = document.createElement("th");
  nameCity.innerText = "City";
  var nameCountry = document.createElement("th");
  nameCountry.innerText = "Country";


  theadTR.appendChild(nameContactName);
  theadTR.appendChild(nameCity);
  theadTR.appendChild(nameCountry);

  thead.appendChild(theadTR);

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  tabla.appendChild(thead);

  for (var i = 0; i < JSONClientes.value.length; i++) {
      var nuevaFila = document.createElement("tr");
      var columnaContactName = document.createElement("td");
      columnaContactName.innerText = JSONClientes.value[i].ContactName;
      var columnaCity = document.createElement("td");
      columnaCity.innerText = JSONClientes.value[i].City;
      var columnaCountry = document.createElement("td");
      var imgBandera = document.createElement("img");
      imgBandera.classList.add("flag");

if ( JSONClientes.value[i].Country=="UK" )
{      imgBandera.src = rutaBandera + "United-Kingdom" + extension;}
else
      {imgBandera.src = rutaBandera + JSONClientes.value[i].Country + extension;}

      imgBandera.alt = JSONClientes.value[i].Country;

      columnaCountry.appendChild(imgBandera);

      nuevaFila.appendChild(columnaContactName);
      nuevaFila.appendChild(columnaCity);
      nuevaFila.appendChild(columnaCountry);
      tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);

}
