var productosObtenidos;

function obtenerProductos()
{

  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();

  request.onreadystatechange = function () {

    //console.log (" Im here!");

    if (this.readyState==4 && this.status == 200 ) {
    //  console.log( request.responseText);
      productosObtenidos = request.responseText;
      procesarProductos();

    }
  }
  request.open("GET", url, true);
  request.send();
}

function procesarProductos()
{
  var JSONProductos = JSON.parse (productosObtenidos);
//  alert(JSONProductos.value[0].ProductName);

  var divTabla = document.getElementById("divTabla");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");
  var thead = document.createElement("thead");
 var theadTR = document.createElement("tr");

  var nameColName = document.createElement("th");
  nameColName.innerText = "Product Name";
    var nameUnitPrice = document.createElement("th");
    nameUnitPrice.innerText = "Unit Price";
      var nameColUnitsInStock = document.createElement("th");
      nameColUnitsInStock.innerText = "Units In Stock";

theadTR.appendChild(nameColName);
theadTR.appendChild(nameUnitPrice);
theadTR.appendChild(nameColUnitsInStock);
thead.appendChild(theadTR);

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

tabla.appendChild(thead);
//  var tablaHead = document.createElement("thead");
//  var trNombre = document.createElement("td");
//  trNombre.value = "Nombre";
//  tabla.appendChild(tablaHead);
  for (var i = 0; i < JSONProductos.value.length; i++) {
      var nuevaFila = document.createElement("tr");
      var columnaNombre = document.createElement("td");
      columnaNombre.innerText = JSONProductos.value[i].ProductName;
      var columnaPrecio = document.createElement("td");
      columnaPrecio.innerText = JSONProductos.value[i].UnitPrice;
      var columnaStock = document.createElement("td");
      columnaStock.innerText = JSONProductos.value[i].UnitsInStock;
      nuevaFila.appendChild(columnaNombre);
      nuevaFila.appendChild(columnaPrecio);
      nuevaFila.appendChild(columnaStock);
      tbody.appendChild(nuevaFila);
//      console.log(JSONProductos.value[i]);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);

}
